## 说明ovs2.3.0-no kernel model
```
该文档内容为openvswitch的编译,打包及运行验证。

** date:20191008,openvswitch-2.3.0 (no kernel mode) **
```
## 安装依赖软件包，及创建普通用户ovs
```
yum -y install wget gcc make python-devel openssl-devel kernel-devel graphviz kernel-debug-devel autoconf automake rpm-build redhat-rpm-config libtool
adduser ovs
su - ovs
```
## 下载源码包，并创建rpm包。
```
mkdir -p ~/rpmbuild/SOURCES
cd ~/rpmbuild/SOURCES
git clone 
sed 's/openvswitch-kmod, //g' openvswitch-2.3.0/rhel/openvswitch.spec > openvswitch-2.3.0/rhel/openvswitch_no_kmod.spec
rpmbuild -bb --nocheck openvswitch-2.3.0/rhel/openvswitch_no_kmod.spec
exit
```
## root用户创建ovs配置目录，并安装制作好的rpm包。
```
# mkdir /etc/openvswitch
# yum localinstall /home/ovs/rpmbuild/RPMS/mips64el/openvswitch-2.3.0-1.mips64el.rpm
```
## 如果selinux开启，请执行如下操作，如未开启，请略过此步骤。
```
yum install policycoreutils-python
semanage fcontext -a -t openvswitch_rw_t "/etc/openvswitch(/.*)?"
restorecon -Rv /etc/openvswitch
```
## 验证 
### 启动服务
```
#chkconfig openvswitch on
#systemctl restart openvswitch.service
```
### 执行API
```
查看ovs版本信息:ovs-vsctl -V
[root@loongson ovs]# ovs-vsctl -V
ovs-vsctl (Open vSwitch) 2.3.0
Compiled Sep 21 2019 16:45:48
DB Schema 7.6.0
```
